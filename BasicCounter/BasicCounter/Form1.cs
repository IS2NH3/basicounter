﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClasCompteur
{
    public class Compteur
    {
        private int cpt;

        public Compteur()
        {
            cpt = 0;
        }
        public string incrementation()
        {
            cpt++;
            return string.Format("{0}", cpt);
        }
        public string decrementation()
        {
            cpt--;
            return string.Format("{0}", cpt);
        }
        public string raz()
        {
            cpt = 0;
            return string.Format("{0}", cpt);
        }

        public int getnb()
        {
            return this.cpt;
        }
    }
}
